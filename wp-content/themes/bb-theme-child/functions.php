<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
    wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
});

 

// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'footer-1' => __( 'Footer Menu 1' ),
            'footer-2' => __( 'Footer Menu 2' ),
            'footer-3' => __( 'Footer Menu 3' ),
            'footer-4' => __( 'Footer Menu 4' ),
            'footer-5' => __( 'Footer Menu 5' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );


 
// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');

 


// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');





function fr_img($id=0,$size="",$url=false,$attr=""){

    //Show a theme image
    if(!is_numeric($id) && is_string($id)){
        $img=get_stylesheet_directory_uri()."/images/".$id;
        if(file_exists(to_path($img))){
            if($url){
                return $img;
            }
            return '<img src="'.$img.'" '.($attr?build_attr($attr):"").'>';
        }
    }

    //If ID is empty get the current post attachment id
    if(!$id){
        $id=get_post_thumbnail_id();
    }

    //If Id is object it means that is a post object, thus retrive the post ID
    if(is_object($id)){
        if(!empty($id->ID)){
            $id=$id->ID;
        }
    }

    //If ID is not an attachment than get the attachment from that post
    if(get_post_type($id)!="attachment"){
        $id=get_post_thumbnail_id($id);
    }

    if($id){
        $image_url=wp_get_attachment_image_url($id,$size);
        if(!$url){
            //If image is a SVG embed the contents so we can change the color dinamically
            if(substr($image_url,-4,4)==".svg"){
                $image_url=str_replace(get_bloginfo("url"),ABSPATH."/",$image_url);
                $data=file_get_contents($image_url);
                echo strstr($data,"<svg ");
            }else{
                return wp_get_attachment_image($id,$size,0,$attr);
            }
        }else if($url){
            return $image_url;
        }
    }
}


//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );



remove_filter( 'wpseo_breadcrumb_links', 'wpse_100012_override_yoast_breadcrumb_trail' );

// //Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter( 'wpseo_breadcrumb_links', 'wpse_100012_override_yoast_breadcrumb_trail_two' );

function wpse_100012_override_yoast_breadcrumb_trail_two( $links ) {
    global $post;

    if ( is_singular( 'hardwood_catalog' )  ) {
        
        $breadcrumb[] = array(
            'url' => get_site_url().'/hardwood/',
            'text' => 'Hardwood',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/hardwood/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );

    }else if (is_singular( 'carpeting' )) {
        
        $breadcrumb[] = array(
            'url' => get_site_url().'/carpet/',
            'text' => 'Carpet',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/carpet/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );

    }else if (is_singular( 'luxury_vinyl_tile' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/vinyl/',
            'text' => 'Vinyl',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/vinyl/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );
        
    }else if (is_singular( 'laminate_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/laminate/',
            'text' => 'Laminate',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/laminate/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );
        
    }else if (is_singular( 'tile_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/tile/',
            'text' => 'Tile',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/tile/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );
        
    }else if (is_singular( 'solid_wpc_waterproof' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/waterproof-flooring/',
            'text' => 'Waterproof Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/waterproof-flooring/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );
    }

    return $links;
}

add_filter( 'facetwp_indexer_row_data', function( $rows, $params ) {
    if ( 'fiber' == $params['facet']['name'] ) {
        $post_id = (int) $params['defaults']['post_id'];
        $field_value = get_post_meta( $post_id, 'fiber', true );
        $new_row = $params['defaults'];
        $Nylon = array("Anso R2x","Kashmere","Caress By Shaw R2x","Unbranded","Nylon R2x","Bellera",
                                "Endura 3","Stainmaster Deluxe","Stainmaster Premier","Wear-Dated","Endura 3 Sd",
                                "ColorStrand® Nylon","Duracolor® Premium Nylon","Pet R2x","Wear-Dated Allure",
                                "Cleartouch Platinum","Cleartouch Plat Ld","Ansoex R2x","Clrtchptnd",
                                "Stainmaster Extra Life Tactess","Cleartouch","Anso R2x (old Caress)","ColorStrand® SD Nylon",
                                "Stainmaster","Anso Rna R2x","Solutia Ultron","Wear-Dated SoftTouch");
        $Wool = array("New Zealand Wool","New Zealand wool","Wool and SmartStrand Silk","Wool and Wool Blends","Wool and SmartStrand Blend");
        $Polypropylene = array("EverStrand BCF","EverStrand BCF/Triexta Blend","Smpetprtct","Permastrand","PermaStrand","Air.O",
                            "Evertouch R2x","S Safety","Stain Resist Hp","EverStrand BCF PET","EverStrand Soft Appeal",
                            "EverStrand","100% Sd Pet Polyester","UltraStrand","100% Pet Polyester");
                            
        $SmartStrand = array("SmartStrand w/DuPont Sorona Ultra","SmartStrand Silk w/DuPont Sorona","SmartStrand",
                        "Forever Fresh Ultrasoft","SmartStrand Forever Clean Silk","SmartStrand Silk PET Blend","SmartStrand Sorona",
                        "SmartStrand PET Blend","Solution Q","SmartStrand Silk Reserve","SmartStrand Forever Clean Ultra",
                        "Triexta PET Blend","SmartStrand Ultra PET Blend","SmartStrand w/DuPont Sorona");                    
        
        if(in_array(trim($field_value) , $Nylon)){
            $new_row['facet_value'] = "Nylon";
            $new_row['facet_display_value'] = "Nylon"; 
         }else if(in_array(trim($field_value) , $Wool)){
            $new_row['facet_value'] = "Wool";
            $new_row['facet_display_value'] = "Wool"; 
         }else if(in_array(trim($field_value) , $Polypropylene)){
            $new_row['facet_value'] = "Polypropylene";
            $new_row['facet_display_value'] = "Polypropylene"; 
         }else if(in_array(trim($field_value) , $SmartStrand)){
            $new_row['facet_value'] = "SmartStrand";
            $new_row['facet_display_value'] = "SmartStrand"; 
         }else{
            $new_row['facet_value'] = $field_value; // value
            $new_row['facet_display_value'] = $field_value; // label
         }
         $rows =array();
        $rows[] = $new_row;
    }
    return $rows;
}, 10, 2 );